# projet_git

faazddeefef
uaehukolm^test double commit
aza


## Table of Contents
1. [General Info](#general-info)
2. [Technologies](#technologies)
3. [Installation](#installation)
4. [Collaboration](#collaboration)
5. [FAQs](#faqs)
### General Info
***
Sent on an expert mission with two of our employees to set up best practices on a fresh project acquired by the client company and thus encourage the addition of new functionalities by a team that will be put together after our visit.
## Technologies
***
A list of technologies used within the project:
* [PHP](https://www.php.net/): Version 8
* [Docker](https://www.docker.com/): Version 20.10
## Installation
***
The customer us asks to put in place a maximum of best practices, in particular by allowing new developers a clear installation, with few commands, but also minimal documentation, a duplication of repositories source code, etc. In addition to this list, there are any recommendations that we can offer, in terms of workflow and other work items.
```
$ git clone https://example.com
$ cd ../path/to/the/file
$ npm install
$ npm start
```
Side information: To use the application in a special environment use ```lorem ipsum``` to start
## Collaboration
***
First of all setup docker then install php

> Create Dockerfile.
>
> Create docker-compose.yml

Php and composer must be present on the docker-compose.yml
## FAQs
***
A list of frequently asked questions
1. **How to execute Dockerfile**
   > docker build -t <user-dockerhub>/<image-name>:tag /path/
2. **How to execute docker-compose.yml**>>To answer this question we use an unordered list:
* build image
  > docker-compose build
* start container
  > docker-compose up
* stop container
   > docker-composer down
